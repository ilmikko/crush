# We need this to load lib.sh at start.
. $1;

test_bootstrap(){
	assert "$(bootstrap)" "$BOOTSTRAP";
}

test_crush_file_with_dividers(){
	want="$BOOTSTRAP
<~# $ONE $ONE_MOD
#one
<~# $DIV $DIV_MOD
#<~# divider
#divider"
	assert "$(crush "$ONE" "$DIV")" "$want";
}

test_crush_no_files(){
	# Crushing an empty set should still yield the bootstrap, not sure why you would do this though.
	assert "$(crush 2>&1)" "$BOOTSTRAP";
}

test_crush_nonexistent_file(){
	assert "$(crush "$NONEXISTENT" 2>&1)" "File does not exist: $NONEXISTENT";
}

test_crush_one_file(){
	want="$BOOTSTRAP
<~# $ONE $ONE_MOD
#one"
	assert "$(crush "$ONE")" "$want";
}

test_crush_two_files(){
	want="$BOOTSTRAP
<~# $TWO $TWO_MOD
#two
<~# $ONE $ONE_MOD
#one"
	assert "$(crush "$TWO" "$ONE")" "$want";
}

test_usage_string(){
	# crush-lib-test is $0 for the test.
	assert "$(usage)" "Usage: crush-lib-test [file [file [...]]]";
}
