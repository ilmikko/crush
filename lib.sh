DIVIDER="<~#";

bootstrap(){
	echo "DIVIDER='$DIVIDER'";
	cat $dir/bootstrap.sh;
	echo "$COMMANDS";
	echo "exit;";
}

crush(){
	for file in $@; do
		if [ ! -f "$file" ]; then
			echo "File does not exist: $file" 1>&2;
			exit 1;
		fi
	done
	bootstrap;
	# Make folder structures first.
	for file in $@; do
		printf "$DIVIDER %s\n%s\n" "$DIRECTORY$(perms $file)" "$(cat $file | awk '{ print "#"$0 }')";
	done
}

expand(){
	# Expand folders, check for nonexistent files.
	for file in $@; do
		if [ -f "$file" ]; then
			echo $file;
		elif [ -d "$file" ]; then
			expand $file/*;
		else
			case $file in
				*\*)
					;;
				*)
					echo $file;
					;;
			esac
		fi
	done
}

perms(){
	stat -c "%n %a" "$file";
}

usage(){
	echo "Usage: $(basename $0) [file [file [...]]]";
}
