# PREREQUISITE COMMANDS
# You can run these commands yourself, or run this file.

# Use .githooks as the git hooks.
# This prevents code that fails the tests to be pushed into prod.
git config core.hooksPath .githooks
