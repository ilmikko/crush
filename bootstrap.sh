if [ ! -f $0 ]; then
	echo;
	echo 'Extracting source...';
	tmp=$(mktemp);
	trap "rm $tmp" EXIT;
	chmod +x $tmp;
	cat >> $tmp;
	DIVIDER=$DIVIDER exec $tmp;
fi
echo 'Extracting installation...';
awk '{ if ($1=="'"$DIVIDER"'") { print $3,$2 } }' $0 | while read line; do
file=$(echo "$line" | awk '{ print $2 }');
mkdir -p $(dirname $file);
touch $file;
chmod $line;
done;
awk '{ if ($1=="'"$DIVIDER"'") { file=$2 } else if (file) { print(substr($0,2)) > file } }' $0;
