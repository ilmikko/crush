#!/usr/bin/env bash

# Test that cru.sh without any arguments exits with code 255.
test_no_arguments_passed(){
	assert_exec $1 255;
	assert_find "$($1)" "Usage";
}

test_unknown_command(){
	assert_exec "$1 --nonexistent" 1;
	assert_find "$($1 --nonexistent)" "nonexistent";
}

# Test that we work in the right directory.
test_right_dir(){
	# Try to crush after cd-ing into an arbitrary directory.
	tmpdir=$(mktemp --directory tmp.XXXXXX);
	echo "This is a test." > $tmpdir/test.txt;

	$1 $tmpdir/test.txt > compressed.sh;
	rm $tmpdir/test.txt;

	chmod +x compressed.sh;
	assert_exec "compressed.sh";
	assert_file $tmpdir/test.txt;

	rm $tmpdir/test.txt;
	rmdir $tmpdir;
	rm compressed.sh;
}

# Test that directories get created if we do something like cru.sh dir/other/dir/one.txt
test_right_new_dirs(){
	tmpdir=$(mktemp --directory tmp.XXXXXX);
	mkdir -p $tmpdir/one/two/three;
	echo "File one." > $tmpdir/one/test.txt;
	echo "File two." > $tmpdir/one/two/test.txt;
	echo "File three." > $tmpdir/one/two/three/test.txt;

	$1 $tmpdir/one/test.txt $tmpdir/one/two/test.txt $tmpdir/one/two/three/test.txt > compressed.sh;
	rm $tmpdir/one/test.txt $tmpdir/one/two/test.txt $tmpdir/one/two/three/test.txt;
	rmdir $tmpdir/one/two/three $tmpdir/one/two $tmpdir/one $tmpdir;

	chmod +x compressed.sh;
	assert_exec "compressed.sh";
	assert_file $tmpdir/one/test.txt;
	assert_file $tmpdir/one/two/test.txt;
	assert_file $tmpdir/one/two/three/test.txt;

	rm $tmpdir/one/test.txt $tmpdir/one/two/test.txt $tmpdir/one/two/three/test.txt;
	rmdir $tmpdir/one/two/three $tmpdir/one/two $tmpdir/one $tmpdir;
	rm compressed.sh;
}

# Test the --directory option.
test_different_directory(){
	tmpdir=$(mktemp --directory tmp.XXXXXX);
	mkdir -p $tmpdir/one/two/three;
	echo "File one." > $tmpdir/one/test.txt;
	echo "File two." > $tmpdir/one/two/test.txt;
	echo "File three." > $tmpdir/one/two/three/test.txt;

	$1 --directory "some/other/dir" $tmpdir/one/test.txt $tmpdir/one/two/test.txt $tmpdir/one/two/three/test.txt > compressed.sh;
	rm $tmpdir/one/test.txt $tmpdir/one/two/test.txt $tmpdir/one/two/three/test.txt;
	rmdir $tmpdir/one/two/three $tmpdir/one/two $tmpdir/one $tmpdir;

	chmod +x compressed.sh;
	assert_exec "compressed.sh";

	assert_file some/other/dir/$tmpdir/one/test.txt;
	assert_file some/other/dir/$tmpdir/one/two/test.txt;
	assert_file some/other/dir/$tmpdir/one/two/three/test.txt;

	rm some/other/dir/$tmpdir/one/test.txt some/other/dir/$tmpdir/one/two/test.txt some/other/dir/$tmpdir/one/two/three/test.txt;
	rmdir some/other/dir/$tmpdir/one/two/three some/other/dir/$tmpdir/one/two some/other/dir/$tmpdir/one some/other/dir/$tmpdir some/other/dir some/other some;
	rm compressed.sh;
}

test_run_command(){
	tmpdir=$(mktemp --directory tmp.XXXXXX);

	echo "File one." > $tmpdir/test.txt;

	$1 $tmpdir/test.txt --command "touch touched.txt" > compressed.sh;
	chmod +x "compressed.sh";
	assert_exec "compressed.sh";
	assert_file touched.txt;

	rm $tmpdir/test.txt touched.txt;
	rm compressed.sh;
	rmdir $tmpdir;
}
