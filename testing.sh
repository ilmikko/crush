# Simple equals assertion.
assert() {
	if [ "$1" != "$2" ]; then
		echo "ASSERT ERROR";
		[ -z "$3" ] || echo "$3";
		echo "Want: $2";
		echo "Got : $1";
		false;
	else
		true;
	fi
}

# Execution assertion, when expecting a return code.
assert_exec(){
	out=$($1 2>&1); got=$?;
	want=$2; [ -z "$want" ] && want=0;
	if [ $got != $want ]; then
		echo "$out";
		echo "ASSERT_EXEC ERROR";
		echo "While executing \$($1): return code was $got, want $want!";
		exit 1;
	fi
}

# Assert that a file exists.
assert_file(){
	if ! [ -f "$1" ]; then
		echo "ASSERT_FILE ERROR";
		echo "File $1 does not exist!";
		exit 1;
	fi
}

# Assert that the word is found in a string.
assert_find(){
	if ! echo $1 | grep -q $2; then
		echo "ASSERT_FIND ERROR";
		echo "Cannot find '$2' in '$1'!";
	fi
}

# Some testing variables.
BOOTSTRAP="$(cat "bootstrap.sh")";

# Run libraries passed by bazel.
testing=$1;
shift;
. $testing $@;

# Initialize some test files.
ONE=$(mktemp);
TWO=$(mktemp);
DIV=$(mktemp);
ONE_MOD=711;
TWO_MOD=622;
DIV_MOD=700;
NONEXISTENT="./non/exis/tent.file"

echo one > "$ONE";
echo two > "$TWO";
echo "$DIVIDER divider
divider" > "$DIV";

chmod "$ONE_MOD" "$ONE";
chmod "$TWO_MOD" "$TWO";
chmod "$DIV_MOD" "$DIV";

# Convert args to full paths.
p=$1;
shift;
while [ $# -gt 0 ]; do
	p="$(realpath $p)"\ "$1";
	shift;
done

# Run only methods starting with test_.
for f in $(declare -F | cut -d ' ' -f3); do
	[[ "$f" == test_* ]] || continue;
	out=$($f $p && true);
	if [ -z "$out" ]; then
		echo "PASS $f";
	else
		echo "FAIL $f:";
		echo "$out";
		exit 1;
	fi
done

# Remove the test files.
rm $ONE $TWO;
