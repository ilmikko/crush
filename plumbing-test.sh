#!/usr/bin/env bash

tmpfile=$(mktemp);
chmod +x "$tmpfile";

# Test that cru.sh with arguments works correctly.
test_plumbing_0_crush(){
	# cru.sh one.txt two.txt
	$1 "$ONE" "$TWO" > $tmpfile;
	rm "$ONE" "$TWO";
	assert_file "$tmpfile";
}

test_plumbing_1_contents(){
	got=$(cat "$tmpfile");
	want="$BOOTSTRAP
<~# $ONE $ONE_MOD
#one
<~# $TWO $TWO_MOD
#two";
	assert "$got" "$want" "Test file contains wrong results!";
}

test_plumbing_2_execution(){
	# Test that the file actually executes properly.
	assert_exec "$tmpfile";
}

test_plumbing_3_files(){
	# Test that the files got extracted.
	assert_file "$ONE";
	assert_file "$TWO";
}

test_plumbing_4_permissions(){
	# Test that the permissions are correct.
	got="$(stat -c "%a" "$ONE") $(stat -c "%a" "$TWO")";
	want="$ONE_MOD $TWO_MOD";
	assert "$got" "$want" "Extracted file(s) have wrong permissions!";
}

test_plumbing_5_file_data(){
	# Test that the files contain the right data.
	got="$(cat $ONE) $(cat $TWO)";
	want="one two";
	assert "$got" "$want" "Extracted file(s) contain wrong results!";
}

test_plumbing_6_piping(){
	# Quick test that extraction works when piping as well.
	rm "$ONE" "$TWO";
	if ! cat "$tmpfile" | bash 2>&1 1>/dev/null; then
		echo "Extraction when piping to bash doesn't execute properly!";
		exit 1;
	fi
	assert_file "$ONE";
	assert_file "$TWO";
	rm $tmpfile;
}
